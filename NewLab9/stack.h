#pragma once

template<class T>
class Stack
{
private:
	StackNode<T> * makeNode(T data);
	StackNode<T> * root;
	void DestructorHelper(StackNode<T> * curNode); // recursive
	void CopyHelper(StackNode<T> * curNode); //recursive

public:
	T Pop();
	Stack();
	void PrintStack();
	void Push(T data);
	Stack(Stack<T> const &original);
	~Stack();
};




template<class T>
inline void Stack<T>::DestructorHelper(StackNode<T>* curNode)
{
}

template<class T>
inline void Stack<T>::CopyHelper(StackNode<T>* curNode)
{
}

template<class T>
inline T Stack<T>::Pop()
{
	return T();
}

template <class T>
Stack<T>::Stack()
{


}

template <class T>
void Stack<T>::PrintStack()
{

}

template <class T>
void Stack<T>::Push(T data)
{

}

template <class T>
Stack<T>::Stack(Stack<T> const &original)
{

}

template <class T>
Stack<T>::~Stack()
{


}